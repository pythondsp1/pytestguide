PyTest Guide
============

This is the short tutorial on writing and testing the simulators using py.test and numpy.testing. 
Also, correct procedure for designing the simulators is added in the section :ref:`goodPractices`. 

Code to test
------------

Following is the code which we want to test. 

.. code-block:: python

    # mathEx.py


    def stat2Num(x, y):
        """ Sum and mean value of two numbers

        Parameters
        ----------
        x : float
            first number to be added

        y : float
            second number two be added

        Returns
        -------
        x+y: float
            sum of two numbers
        (x+y)/2 : float
            mean of two numbers
        """
        return(x+y, (x+y)/2)

Test Code
---------


Following is the testing code, 

.. code-block:: python

    # test_mathEx.py

    from mathEx import stat2Num


    def test_stat2Num():
    """ Test for stat2Num """

        assert stat2Num(3, 2) == (5, 2.5)
        assert stat2Num(3, 2.0) == (5, 2.5)
        assert stat2Num(3.5, 2.5) == (6, 3)
        assert stat2Num(3.5, 2.5) == (6.0, 3.0)


Run any of the following commands to run the test, 

* **py.test** : it runs all the tests in the directory, which start with name 'test' or classes which start with name 'Test'. Note that, these names are case sensitive i.e. we can not use test and Test interchangeably.  
* **py.test filename.py** : runs all the test in the filename.py
* **pytext -x** : runs the test and stops as soon as the test fails.

.. code-block:: shell

    $ py.test
    $ py.test test_mathEx.py
    $ py.test -x
    ========== test session starts ========
    [...] 
    collected 1 items 

    test_mathEx.py .


One dot after test_mathEx.py represents that one test is passed. 

* **pytext -v** : it is same as above commands but results are verbose, i.e. gives some details about the results. 

.. code-block:: shell

    $ pytest -v
    ========== test session starts ========
    [...] 
    collected 1 items 

    test_mathEx.py::test_stat2Num PASSED


* **py.test - -collect-only** : only show the list of test (does not perform any test). Run following command to see the list of test, 

.. code-block:: shell

    $ py.test --collect-only
    =========== test session starts ========
    [...]
    collected 1 items 
    <Module 'test/test_mathEx.py'>
      <Function 'test_stat2Num'>


Also, we can group these commands as follows, 

.. code-block:: shell

    $ py.test -x -v

For more details of command, use help funciton as below, 

.. code-block:: shell

    $ py.test -h

Test can be group in the class as follows, 

.. code-block:: python

    # test_mathEx.py

    from mathEx import stat2Num


    def test_stat2Num():
        """ Test method for stat2Num """

        assert stat2Num(3, 2) == (5, 2.5)
        assert stat2Num(3, 2.0) == (5, 2.5)
        assert stat2Num(3.5, 2.5) == (6, 3)
        assert stat2Num(3.5, 2.5) == (6.0, 3.0)


    class Test_stat2Num(object):
        """ Test class for stat2Num """

        def test_stat2Num_inputInt(self):
            """ Test with integer inputs """
            assert stat2Num(3, 2) == (5, 2.5)
            assert stat2Num(3, 2) == (5.0, 2.5)

        def test_stat2Num_inputFloat(self):
            """ Test with float inputs """
            assert stat2Num(3, 2) == (5, 2.5)
            assert stat2Num(3, 2) == (5.0, 2.5)


.. code-block:: shell

    $ pytest -v
    ========== test session starts ========
    [...] 
    collected 3 items 

    test_mathEx.py::test_stat2Num PASSED
    test_mathEx.py::Test_stat2Num::test_stat2Num_inputInt PASSED
    test_mathEx.py::Test_stat2Num::test_stat2Num_inputFloat PASSED



Testing Cython Code
-------------------

First write cython code as below, 

.. code-block:: cython

    # mathCy.pyx

    import pyximport
    pyximport.install()

    cpdef (double, double) stat2NumCy(double x, double y):
        """ Sum and mean value of two numbers

        Parameters
        ----------
        x : double
            first number to be added

        y : double
            second number two be added

        Returns
        -------
        x+y: double
            sum of two numbers
        (x+y)/2 : double
            mean of two numbers
        """
        return(x+y, (x+y)/2)

Next, add setup.py as below, 

.. code-block:: cython

    from distutils.core import setup
    from Cython.Build import cythonize

    setup(
      name = 'pytest',
      ext_modules = cythonize(["*.pyx"], compiler_directives={
          'embedsignature': True,
          'boundscheck' : False,
          'wraparound' : False}),
    )

Next, run the setup.py file as below, 

.. code-block:: shell

    $ python setup.py build_ext --inplace

Now, add the test code for cython code as follows, 

.. code-block:: python

    # test_mathEx.py

    from mathEx import stat2Num
    from mathCy import stat2NumCy


    [...]


    class Test_stat2Num(object):
        [...] 


    class Test_stat2NumCy(object):
        """ Test class for stat2NumCy """

        def test_stat2Num_inputInt(self):
            """ Test with integer inputs """
            assert stat2NumCy(3, 2) == (5, 2.5)
            assert stat2NumCy(3, 2) == (5.0, 2.5)

        def test_stat2Num_inputFloat(self):
            """ Test with float inputs """
            assert stat2NumCy(3, 2) == (5, 2.5)
            assert stat2NumCy(3, 2) == (5.0, 2.5) 

Mark
----

Marks can be used to run selected tested (instead of all tests). In the following code, tests for the python codes are marked as 'pyfiles', whereas tests for cython codes are marked as 'cyfiles', 

.. code-block:: python

    # test_mathEx.py


    import os
    import sys

    # add math package to path from back-folder
    sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'math'))

    import pytest

    from mathEx import stat2Num
    from mathCy import stat2NumCy

    @pytest.mark.pyfile
    def test_stat2Num():
        """ Test method for stat2Num """

        assert stat2Num(3, 2) == (5, 2.5)
        assert stat2Num(3, 2.0) == (5, 2.5)
        assert stat2Num(3.5, 2.5) == (6, 3)
        assert stat2Num(3.5, 2.5) == (6.0, 3.0)

    @pytest.mark.pyfile
    class Test_stat2Num(object):
        """ Test class for stat2Num """

        def test_stat2Num_inputInt(self):
            """ Test with integer inputs """
            assert stat2Num(3, 2) == (5, 2.5)
            assert stat2Num(3, 2) == (5.0, 2.5)

        def test_stat2Num_inputFloat(self):
            """ Test with float inputs """
            assert stat2Num(3, 2) == (5, 2.5)
            assert stat2Num(3, 2) == (5.0, 2.5) 

    @pytest.mark.cyfile
    class Test_stat2NumCy(object):
        """ Test class for stat2NumCy """

        def test_stat2Num_inputInt(self):
            """ Test with integer inputs """
            assert stat2NumCy(3, 2) == (5, 2.5)
            assert stat2NumCy(3, 2) == (5.0, 2.5)

        def test_stat2Num_inputFloat(self):
            """ Test with float inputs """
            assert stat2NumCy(3, 2) == (5, 2.5)
            assert stat2NumCy(3, 2) == (5.0, 2.5) 


After setting the different marks, we can run the one or more selected marks as follows, 

.. code-block:: shell

    $ py.test -m pyfile
    [...]
    === 2 tests deselected by "-m 'pyfile'" ========
    === 3 passed, 2 deselected in 0.11 seconds =====

    $ py.test -m cyfile
    [...]

    === 3 tests deselected by "-m 'cyfile'" ========
    === 2 passed, 3 deselected in 0.06 seconds =====

    $ py.test -m "not pyfile"
    [...]
    === 3 tests deselected by "-m 'cyfile'" ========
    === 2 passed, 3 deselected in 0.06 seconds =====

    $ py.test -m "cyfile or pyfile"
    [...]
    === 5 passed in 0.04 seconds ==============



Parameterized test
------------------

Add following code to end of previous code; and then run the code. In parametrize test, all the sets of inputs will be checked. Note that, '**parametrize**' is the special keyword for testing the code with various parameter.  

.. code-block:: python

    [...]
    @pytest.mark.cyfile
    [...] 

    @pytest.mark.parametrize("x, y", [(3,2), (3, 2.0), (3.5, 2.5)])
    def test_stat2Num(x, y):
        """ Parameterized test method for stat2Num """

        assert stat2Num(x, y) == (x+y, (x+y)/2)

Then, run the test using any of the following commands, 

.. code-block:: shell

    $ py.test -m parametrize
    [...]
    collected 7 items 

    test_mathEx.py ...

    === 4 tests deselected by "-m 'parametrize'" ====
    === 3 passed, 4 deselected in 0.06 seconds ======

    $ py.test
    [...]
    collected 7 items 

    test_mathEx.py .......


Numpy Testing
=============

Numpy testing can be used to checking approximate-equalilty of the results along with various other equalities e.g. equal arrays, almost equal and array-less etc. 

division function for testing
-----------------------------


First, add following code to mathEx.py file, 

.. code-block:: python

    # mathEx.py

    def stat2Num(x, y):
    [...]

    def div2Num(x, y):
        """ div2Num is tested by numpy.testing 
        """
        return(x/y)


test code
---------

Add following code to test_mathEx.py file, 

.. code-block:: python
    
    # test_mathEx.py

    import numpy as np
    from mathEx import stat2Num, div2Num
    [...] 

    @pytest.mark.parametrize("x, y", [(3,2), (3, 2.0), (3.5, 2.5)])
    [...]

    @pytest.mark.numpyfile
    class Test_numpy(object):
        """ check approximate-equalilty conditions using numpy.testing """

        def test_div2Num_pytest(self):
            # 0.66667 != 0.6666666666666666
            assert div2Num(2, 3) != 0.66667

        def test_dif2Num_numpy_4places(self):
            #  2/3 = 0.6666666666666666 = 0.66667 i.e. equal upto 4 places
            np.testing.assert_approx_equal(div2Num(2, 3), 0.66667, 4)

        def test_dif2Num_numpy_5places(self):
            # 2/3 = 0.6666666666666666 = 0.66667 i.e. equal upto 5 places
            # note that it is round off for last value i.e.
            # 0.6666666666666666 is changed to 0.66667 before comparision
            np.testing.assert_approx_equal(div2Num(2, 3), 0.66667, 5)

        def test_dif2Num_numpy_6places(self):
            # failed because
            # 2/3 = 0.6666666666666666 = 0.666667 != 0.666668 upto 6 places
            np.testing.assert_approx_equal(div2Num(2, 3), 0.666668, 6)


Numpy testing is useful when we want to check approximate-equalilty along with some other equalities. In the above code, 'test_div2Num_pytest' will pass because pytest checks eqality for all the decimal values. Hence, it will found that 0.6666666666666666 is not equal to 0.66667. Next, 2 test will be passed and the last test will be failed (see comments for more details). 

Run test
--------

Finally, run the test using following command, 

.. code-block:: shell

    $ py.test -m numpyfile -v
    ========= test session starts ============================
    [...] 
    collected 11 items 

    test_mathEx.py::Test_numpy::test_div2Num_pytest PASSED
    test_mathEx.py::Test_numpy::test_dif2Num_numpy_4places PASSED
    test_mathEx.py::Test_numpy::test_dif2Num_numpy_5places PASSED
    test_mathEx.py::Test_numpy::test_dif2Num_numpy_6places FAILED

    ================================= FAILURES ===========================
    [...]

        def test_dif2Num_numpy_6places(self):
            # failed because
            # 2/3 = 0.6666666666666666 = 0.666667 != 0.666668 upto 6 places
    >       np.testing.assert_approx_equal(div2Num(2, 3), 0.666668, 6)
    E       AssertionError: 
    E       Items are not equal to 6 significant digits:
    E        ACTUAL: 0.6666666666666666
    E        DESIRED: 0.666668

    test_mathEx.py:85: AssertionError
    ================== 7 tests deselected by "-m 'numpyfile'" ===========
    ============= 1 failed, 3 passed, 7 deselected in 0.26 seconds ======



.. _goodPractices:

Good Practices
==============

In previous sections, we created some codes and tested those codes using py.test and numpy.testing. 
Now, we will see the correct methods to create a simulator for the research works. Here, we will rewrite the above codes with philosophy '**First test then implement**', 

Create empty method
-------------------

Fisrt create the mathEx.py file, with a method 'stat2Num' which returns nothing, i.e. do not implement the code as shown below, 

.. code-block:: python

    # mathEx.py

    def stat2Num (x, y):
        pass

Decide input and output parameters
----------------------------------

Decide all the input and output parameters for the above method and write those parameters as comments using 'Numpy docstring style' as below, 

.. code-block:: python

    # mathEx.py

    def stat2Num (x, y):
        """ Sum and mean value of two numbers

        Parameters
        ----------
        x : float
            first number to be added

        y : float
            second number two be added

        Returns
        -------
        x+y: float
            sum of two numbers
        (x+y)/2 : float
            mean of two numbers
        """
        pass

.. note::

    Note that, Numpy docstring style is used for comments, because Python-sphinx software can automatically generate the documents based on docstrings. Therefore, our codes become more readable and others can use theses codes easily. 

Write tests
-----------

After writing docstring, we know the input and output parameter and their types. Next, write the test file 'test_mathEx.py', which contains various tests for the method stat2Num as below. 

.. note::
    
    Tests are very important and can save a lot of time to verify the outputs for various types of inputs. Also, well tested codes encourage others to use your code. Therefore, spend more time in writing tests. 

.. code-block:: python

    # test_mathEx.py

    from mathEx import stat2Num


    def test_stat2Num():
    """ Test for stat2Num """

        assert stat2Num(3, 2) == (5, 2.5)
        assert stat2Num(3, 2.01) != (5, 2.5)
        assert stat2Num(3, 2.0) == (5, 2.5)
        assert stat2Num(3.5, 2.5) == (6, 3)
        assert stat2Num(3.5, 2.5) == (6.0, 3.0)


Run the test after writing it, and it will fail because we did not implement the logic yet, 

.. code-block:: shell

    $ py.test
    ========== test session starts ==========
    [...] 

    test_mathEx.py F

    ============= FAILURES ==================
    [...]

    test_mathEx.py:9: AssertionError


Implement method
----------------

Next, add logic to implement the method in 'mathEx.py' as below, 

.. code-block:: python

    # mathEx.py

    def stat2Num (x, y):
        """ Sum and mean value of two numbers

        Parameters
        ----------
        x : float
            first number to be added

        y : float
            second number two be added

        Returns
        -------
        x+y: float
            sum of two numbers
        (x+y)/2 : float
            mean of two numbers
        """
        return (x+y, x+y/2)


Run test and fix the error
--------------------------

Now, run the test as below. In the above code, average value is calculated in the wrong way i.e. x+y/2, instead of (x+y)/2.

.. code-block:: shell

    $ py.test
    [...]
    test_mathEx.py F
    [...]
        
    >       assert stat2Num(3, 2) == (5, 2.5)
    E       assert (5, 4.0) == (5, 2.5)
    E         At index 1 diff: 4.0 != 2.5
    E         Use -v to get the full diff
 
Error shows that, 4.0 != 2.5. Now, we need to fix the error as below, 

.. code-block:: python
    :linenos:

    # mathEx.py

    def stat2Num (x, y):
        """ Sum and mean value of two numbers

        Parameters
        ----------
        x : float
            first number to be added

        y : float
            second number two be added

        Returns
        -------
        x+y: float
            sum of two numbers
        (x+y)/2 : float
            mean of two numbers
        """
        return (x+y, (x+y)/2)

Now, run the test again, 

.. code-block:: shell

    $ py.test
    [...] 

    test_mathEx.py .
    ========= 1 passed in 0.01 seconds =======


Pep8 writing guidelines
-----------------------

Finally, check whether writing style for the code follows the pep8 guidelines or not,

.. code-block:: shell

    $  pep8 mathEx.py 
    mathEx.py:3:1: E302 expected 2 blank lines, found 1
    mathEx.py:3:13: E211 whitespace before '('
    mathEx.py:21:26: W292 no newline at end of file


Currently, it shows three errors in writing style. Now fix the error as below,  

* First error says that there must be two blank line between function definition and comment at line 1. Hence, press enter after line 1. 
* Second error is 'whitespace before (', i.e. change 'stat2Num (x, y)'' to 'stat2Num(x, y)' at line 3. 
* Similarly, we can remove white space from last i.e. change 'return (x+y, (x+y)/2)' to 'return(x+y, (x+y)/2)'. 
* Last error shows that 'no newline at the end of file' i.e. press enter at the end of the code. 
  
The modified code will look as below, 

.. code-block:: python

    # mathEx.py


    def stat2Num(x, y):
        """ Sum and mean value of two numbers

        Parameters
        ----------
        x : float
            first number to be added

        y : float
            second number two be added

        Returns
        -------
        x+y: float
            sum of two numbers
        (x+y)/2 : float
            mean of two numbers
        """
        return(x+y, (x+y)/2)


Now run the pep8 command again, it will not show any error, 

.. code-block:: shell

    $ pep8 mathEx.py



.. More topics
.. ===========

.. Skip tests
.. ----------

.. * skipif
.. * xfail
.. * Xfail



.. Fixtures
.. --------

